'use strict';

// We merely need the type definitions here:
// tslint:disable-next-line:no-implicit-dependencies
import { APIGatewayEvent, Callback, Context, Handler } from 'aws-lambda';

import {
  PostRefreshJwtRequest,
  PostVerifyOrcidRequest,
} from '../../../lib/interfaces/endpoints/accounts';

import { initialise, Request } from '../../../lib/lambda/faas';
import { withDatabase } from '../../../lib/lambda/middleware/withDatabase';

import { createNewSession } from './resources/createNewSession';
import { getAccountIdForOrcid } from './resources/getAccountIdForOrcid';
import { getNewJwt } from './resources/getNewJwt';
import { getProfiles } from './resources/getProfiles';
import { linkOrcid } from './resources/linkOrcid';
import { readOrcid } from './resources/readOrcid';
import { readOrcidWork } from './resources/readOrcidWork';
import { searchOrcid } from './resources/searchOrcid';

// If we're running on Heroku, use the database attached to it in the accounts schema.
// The else clause can be removed once we've fully migrated to Heroku.
const connectionString = (process.env.DATABASE_URL)
  /* istanbul ignore next */
  ? `${process.env.DATABASE_URL}?searchpath=accounts`
  // tslint:disable-next-line:max-line-length
  : `postgres://${process.env.database_username}:${process.env.database_password}@${process.env.database_endpoint}/accounts_db`;

export const handler: Handler = (event: APIGatewayEvent, _: Context, callback?: Callback) => {
  const faas = initialise(event, callback);
  faas.get('/profiles/:accountIds', withDatabase(connectionString, 'accounts', getProfiles));
  faas.get('/ids/:orcid', withDatabase(connectionString, 'accounts', getAccountIdForOrcid));
  // Perhaps consider moving the /orcid resources to a separate stack,
  // since they're merely proxies to the ORCID API (because it hasn't set CORS headers).
  faas.get('/orcid/search/:query', searchOrcid);
  faas.get('/orcid/:orcid/work/:putCode', readOrcidWork);
  faas.get('/orcid/:orcid', readOrcid);
  faas.post('/', withDatabase(connectionString, 'accounts', createNewSession));
  faas.post<PostVerifyOrcidRequest>(
    '/:sessionId/orcid/verify/:code',
    withDatabase(connectionString, 'accounts', linkOrcid),
  );
  faas.post<PostRefreshJwtRequest>('/:sessionId/jwt', withDatabase(connectionString, 'accounts', getNewJwt));

  faas.default((context: Request<undefined>) => {
    const tail = `/accounts${context.path}`;

    return Promise.reject(new Error(`Invalid method: ${context.method} ${tail}`));
  });
};

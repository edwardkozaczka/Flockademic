---------------------------------------------------------
--                      WARNING                        --
-- Do not modify this script after merging it to       --
-- `dev`. Only add new, consecutively numbered         --
-- changesets in case the schema needs further         --
-- changes.                                            --
---------------------------------------------------------

-- NOTE: When running locally. Docker Compose won't pick up changes in SQL files unless you recreate the volume:
--       docker-compose rm -v; docker-compose up --build;

BEGIN;
  ALTER TABLE scholarly_article_associated_media ADD COLUMN license TEXT;

  -- Before now it was only possible to upload articles under a CC-BY license:
  UPDATE scholarly_article_associated_media
  SET license='https://creativecommons.org/licenses/by/4.0/';
COMMIT;

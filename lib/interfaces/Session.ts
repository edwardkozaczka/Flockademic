import { Account } from './Account';

export interface Session {
  identifier: string;
  account?: Account;
}

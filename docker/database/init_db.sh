#!/bin/bash
set -e

PGDATABASE=flockademic_local_db;
export PGDATABASE;

echo "Initialising database \`${PGDATABASE}\`";
# Create the database for this stack if it doesn't exist yet (it shouldn't, but to be complete):
psql --dbname=postgres -c "CREATE DATABASE ${PGDATABASE};";

# Provision the databases with the scripts placed in /docker-entrypoint-initstacks.d/
sh /tmp/provision_databases.sh /docker-entrypoint-initstacks.d;
